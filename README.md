Programando en Ruby (En Debian)
==

Para poder trabajar apropiadamente con ruby en Debian, será necesario instalar un manejador de paquetes para asegurarnos de tener exáctamente la versión deseada de ruby y sus gemas (?.

Instalación de un manejador de paquetes (virtualEnv)
--

En este caso la elección fue RVM, ya que es uno de los favoritos de la comunidad.
Otra alternativa es rbenv, siendo más minimal, necesita de plugins para la instalación de ruby en sí mismo.

###### Instalado la clave pública de MPapis (Michael Papis)

Michael Papis es el mantenedor oficial de RVM y su clave pública nos permite instalarlo con mayor seguridad.


```
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
```

###### Versión estable de RVM (Sin Rails)

```
\curl -sSL https://get.rvm.io | bash -s stable --ruby
```
Si quisieramos instalar Rails (Web framework análogo a django) debemos sustituir --ruby por --rails.
Por cualquier problema con este paso, leer [aquí](https://rvm.io/rvm/install#installation-explained)

Luego de esto, agregaremos a ~/.profile (o ~/.bashrc):

```
source /etc/profile
```

Por último debemos agregar al usuario interesado al grupo rvm:

```
addgroup $USER rvm
```

Cerramos la terminal y la abrimos nuevamente.

Para confirmar que la instalación fué correcta, ejecutamos:

```
type rvm | head -n 1
```

Lo cual debería decirnos algo como "rvm: es una función."

###### Instalando una versión específica de Ruby

```
rvm install 2.4.2
```

ahora para utilizar la versión instalada, ejecutamos:

```
rmv use 2.4.2
```

lo podremos verificar con:

```
ruby --version
```

Con esto hemos logrado instalar la versión estable (al momento de escribir este tuto) más reciente de ruby en comaparación con la 2.3 que tenemos por defecto en Debian.

Ruby en 20 Minutos (Paseo por el lenguaje)
--

Dejo este [link](https://www.ruby-lang.org/es/documentation/quickstart/) para tener una breve introducción de las particularidades del lenguaje, en este tutorial enseñan a utilizar la shell, mediante ejercicios prácticos.

Referencias extras
--

[Troubleshooting your instalation](https://rvm.io/rvm/install#troubleshooting-your-install)
[Where to now?](https://rvm.io/rvm/install#where-to-now)

